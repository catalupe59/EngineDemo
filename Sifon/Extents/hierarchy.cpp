#include "stdafx.h"
#include "hierarchy.h"
#include "Core\Components\game_object.h"

Hierarchy::Hierarchy() {
  
}


Hierarchy::~Hierarchy() {

}

void Hierarchy::insert(std::string name, GameObject* object){
  objects.insert(std::pair<std::string,std::unique_ptr<GameObject>>(name, std::unique_ptr<GameObject>(object)));
}

GameObject& Hierarchy::operator[](std::string name){
  return *objects.find(name)->second;
}
GameObject& Hierarchy::operator[](char* name){
  std::string string_name = std::string(name);
  return *objects.find(string_name)->second;
}
