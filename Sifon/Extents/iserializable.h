#pragma once
class ISerializable {
public:
	virtual void serialize(std::string file) = 0;
	virtual void deserialize(std::string file) = 0;
};
