#pragma once


class Observer;
class Observable {
private:
	std::vector<Observer*> observers;
public:
	Observable();
	virtual ~Observable() = 0;

	void notify();
	void add_observer(Observer* new_observer);
	void operator+=(Observer* obs);
};

