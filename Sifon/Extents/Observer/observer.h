#pragma once
class Observer {
public:
	Observer();
	virtual ~Observer() = 0;
	virtual void on_notify() = 0;
};

