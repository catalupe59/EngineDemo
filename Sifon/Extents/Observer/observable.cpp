#include "stdafx.h"
#include "observable.h"
#include "observer.h"

Observable::Observable() {

}


Observable::~Observable() {

}

void Observable::notify() {
	for (auto it = observers.begin(); it != observers.end(); it++) {
		(*it)->on_notify();
	}
}

void Observable::add_observer(Observer * new_observer) {
	observers.push_back(new_observer);
}

void Observable::operator+=(Observer * new_observer) {
	observers.push_back(new_observer);
}
