#pragma once

class GameObject;

class Hierarchy {
 private:
  std::map<std::string,std::unique_ptr<GameObject>> objects;
 public:
  Hierarchy();
  ~Hierarchy();

  void insert(std::string name, GameObject* object);
  GameObject& operator[](std::string name);
  GameObject& operator[](char*  name);
};

