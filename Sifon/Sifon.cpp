// Sifon.cpp : Defines the entry point for the console application.
//

#include "stdafx.h"
#include "Core\Debug\debug.h"
#include "Core\program.h"
#include "Core\System\input_manager.h"
#include "Core\System\screen.h"
#include "Core\Graphics\frame_buffer.h"

struct GLFWwindowKill {
	void operator()(GLFWwindow* window) {
		return glfwDestroyWindow(window);
	}
};


int main() {
#ifdef _DEBUG
	_CrtSetDbgFlag(_CRTDBG_ALLOC_MEM_DF | _CRTDBG_LEAK_CHECK_DF);
#endif
#ifdef _CLIENT_
	if (!glfwInit()) {
		Debug::log("Failed to open GLFW");
		return -1;
	}
	Debug::log("GLFW init successful");
	glfwWindowHint(GLFW_OPENGL_PROFILE, GLFW_OPENGL_COMPAT_PROFILE);
	glfwWindowHint(GLFW_SAMPLES, 4);
	glfwWindowHint(GLFW_CONTEXT_VERSION_MAJOR, 4);
	glfwWindowHint(GLFW_CONTEXT_VERSION_MINOR, 3);
	glfwWindowHint(GLFW_OPENGL_FORWARD_COMPAT, GL_TRUE);

	Screen::set_screen(1366, 768);

	std::unique_ptr<GLFWwindow, GLFWwindowKill> window = std::unique_ptr<GLFWwindow, GLFWwindowKill>(glfwCreateWindow(Screen::get_screen_size().x, Screen::get_screen_size().y, "Tema", /*glfwGetPrimaryMonitor()*/ nullptr, nullptr));

	glfwSetKeyCallback(window.get(), Input_Manager::key_callback);
	glfwSetMouseButtonCallback(window.get(), Input_Manager::mouse_button_callback);
	glfwSetCursorPosCallback(window.get(), Input_Manager::mouse_pos_callback);
	glfwSetInputMode(window.get(), GLFW_CURSOR, GLFW_CURSOR_DISABLED);

	glewExperimental = true;
	glfwMakeContextCurrent(window.get());

	glewExperimental = GL_TRUE;
	GLenum err = glewInit();
	if (GLEW_OK != err) {
		Debug::log("Failed to init glew");
	}
	Debug::log("GLEW init successful");
	
	glEnable(GL_DEPTH_TEST);
	glEnable(GL_CULL_FACE);
	glEnable(GL_TEXTURE_2D); 
	glEnable(GL_BLEND);
	glEnable(GL_MULTISAMPLE);
	glBlendFunc(GL_SRC_ALPHA, GL_ONE_MINUS_SRC_ALPHA);

	std::unique_ptr<Program> program = std::unique_ptr<Program>(DBG_NEW Program());
	glViewport(0, 0, 1366, 768);
	program->init();
	while (program->running) {
		double currentTime = glfwGetTime();
		glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT);
		FrameBuffer::clear_framebuffers();
		glClearColor(0.2, 0.2, 0.2f, 1);
		program->run();
		glfwSwapBuffers(window.get());
		glfwPollEvents();
		if (glfwWindowShouldClose(window.get())) {
			program->running = false;
		}
		double last = glfwGetTime();
		//std::cout << "FPS " << (last - currentTime)<<"\n";
	}


	return 0;
#endif
#ifdef _SERVER_


#endif
}

