#include "stdafx.h"
#include "screen.h"

glm::ivec2 Screen::screen_size = glm::ivec2(0, 0);

void Screen::set_screen(int width, int height) {
	Screen::screen_size = glm::ivec2(width, height);
}

glm::ivec2 Screen::get_screen_size() {
	return screen_size;
}