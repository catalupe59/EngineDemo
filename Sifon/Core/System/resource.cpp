#include "stdafx.h"
#include "resource.h"

#include "Core\Components\Shared_Components\mesh.h"
#include "Core\Components\Shared_Components\Mesh\single_mesh.h"
#include "Core\Graphics\shaders.h"
#include "Core\Components\Shared_Components\material.h"
#include "Core\Scripts\map_generator.h"

#define PATH_TO_MESHES "Models/"
#define PATH_TO_MATERIALS "Materials"

std::map<std::string, std::unique_ptr<Mesh>> Resource::mesh_pool;
std::map<std::string, std::unique_ptr<Material>>Resource::material_pool;
std::map<std::string, std::unique_ptr<Shader>> Resource::shader_pool;


void Resource::init() {
	init_meshes();
	init_materials();
	init_shaders();
}
Shader * Resource::get_shader(const std::string & name) {
	return shader_pool[name].get();
}

Mesh * Resource::get_mesh(const std::string & name) {
	return mesh_pool[name].get();
}

Material * Resource::get_material(const std::string & name) {
	return material_pool[name].get();
}

std::map<std::string, std::unique_ptr<Mesh>>& Resource::get_mesh_pool() {
	return mesh_pool;
}

void Resource::insert_material(const std::string name, Material * material) {
	material_pool.insert(std::pair <std::string, std::unique_ptr<Material>>(name, std::unique_ptr<Material>(material)));
}

void Resource::init_meshes() {
	const std::vector<std::string> locations = {"Models/robokyle.obj", "Models/room.obj", "Models/cube.obj", "Models/cubix.obj","Models/gun.obj" , "Models/wave_quad.obj"};
	for (int i = 0; i < locations.size(); i++) {
		mesh_pool.insert(std::pair<std::string, std::unique_ptr<Mesh>>(locations[i], std::unique_ptr<Mesh>(DBG_NEW Single_Mesh(locations[i]))));
	}
	
	mesh_pool.insert(std::pair<std::string, std::unique_ptr<Mesh>>("Models/quad.obj", std::unique_ptr<Mesh>(DBG_NEW Single_Mesh())));
}

void Resource::init_materials() {
	material_pool.insert(std::pair <std::string, std::unique_ptr<Material>>("Robot", std::unique_ptr<Material>(DBG_NEW Material(std::vector<std::string>{"Textures/robot_kyle_diff.png",
																															  "Textures/robot_kyle_metallic.png",
																															  "Textures/robot_kyle_roughness.png"}))));
	material_pool.insert(std::pair <std::string, std::unique_ptr<Material>>("Prototype", std::unique_ptr<Material>(DBG_NEW Material(std::vector<std::string>{"Textures/prototype_diffuse.png",
																															  "Textures/prototype_mettalic.png",
																															  "Textures/prototype_roughness.png"}))));
	material_pool.insert(std::pair <std::string, std::unique_ptr<Material>>("Floor", std::unique_ptr<Material>(DBG_NEW Material(std::vector<std::string>{"Textures/floor_diffuse.jpg",
																																	"Textures/floor_bump.png",
																																	"Textures/floor_reflectivity.png"}))));

}

void Resource::reload_shader(const std::string& name) {
	shader_pool[name].reset(Shader::create_shader(shader_pool[name]->locations));
}

void Resource::init_shaders() {
	Shader* temp = Shader::create_shader(std::vector<std::string>{"Shaders\\PBR.vertex", "Shaders\\PBR.fragment"});
	shader_pool.insert(std::pair<std::string, std::unique_ptr<Shader>>(temp->renderer_type, std::unique_ptr<Shader>(temp)));
	temp = Shader::create_shader(std::vector<std::string>{"Shaders\\FrameBuffer.vertex", "Shaders\\FrameBuffer.fragment"});
	shader_pool.insert(std::pair<std::string, std::unique_ptr<Shader>>(temp->renderer_type, std::unique_ptr<Shader>(temp)));
	temp =Shader::create_shader(std::vector<std::string>{"Shaders\\Unlit.vertex", "Shaders\\Unlit.fragment"});
	shader_pool.insert(std::pair<std::string, std::unique_ptr<Shader>>(temp->renderer_type, std::unique_ptr<Shader>(temp)));
	temp = Shader::create_shader(std::vector<std::string>{"Shaders\\Unlit_Textured.vertex", "Shaders\\Unlit_Textured.fragment"});
	shader_pool.insert(std::pair<std::string, std::unique_ptr<Shader>>(temp->renderer_type, std::unique_ptr<Shader>(temp)));
	temp = Shader::create_shader(std::vector<std::string>{"Shaders\\Points.vertex", "Shaders\\Points.fragment"});
	shader_pool.insert(std::pair<std::string, std::unique_ptr<Shader>>(temp->renderer_type, std::unique_ptr<Shader>(temp)));
	temp = Shader::create_shader(std::vector<std::string>{"Shaders\\Instanced_Lit.vertex", "Shaders\\Instanced_Lit.fragment"});
	shader_pool.insert(std::pair<std::string, std::unique_ptr<Shader>>(temp->renderer_type, std::unique_ptr<Shader>(temp)));
	temp = Shader::create_shader(std::vector<std::string>{"Shaders\\ndotl.vertex", "Shaders\\ndotl.fragment"});
	shader_pool.insert(std::pair<std::string, std::unique_ptr<Shader>>(temp->renderer_type, std::unique_ptr<Shader>(temp)));
	temp = Shader::create_shader(std::vector<std::string>{"Shaders\\Deffered_PBR.vertex", "Shaders\\Deffered_PBR.fragment"});
	shader_pool.insert(std::pair<std::string, std::unique_ptr<Shader>>(temp->renderer_type, std::unique_ptr<Shader>(temp)));
	temp = Shader::create_shader(std::vector<std::string>{"Shaders\\Ground.vertex", "Shaders\\Ground.fragment"});
	shader_pool.insert(std::pair<std::string, std::unique_ptr<Shader>>(temp->renderer_type, std::unique_ptr<Shader>(temp)));
	temp = Shader::create_shader(std::vector<std::string>{"Shaders\\PBR_instanced.vertex", "Shaders\\PBR_instanced.fragment"});
	shader_pool.insert(std::pair<std::string, std::unique_ptr<Shader>>(temp->renderer_type, std::unique_ptr<Shader>(temp)));
	temp = Shader::create_shader(std::vector<std::string>{"Shaders\\bump_reflectivity.vertex", "Shaders\\bump_reflectivity.fragment"});
	shader_pool.insert(std::pair<std::string, std::unique_ptr<Shader>>(temp->renderer_type, std::unique_ptr<Shader>(temp)));
	temp = Shader::create_shader(std::vector<std::string>{"Shaders\\Gerstner.vertex", "Shaders\\Gerstner.fragment"});
	shader_pool.insert(std::pair<std::string, std::unique_ptr<Shader>>(temp->renderer_type, std::unique_ptr<Shader>(temp)));
	temp = Shader::create_shader(std::vector<std::string>{"Shaders\\gerstner_normal.vertex", "Shaders\\gerstner_normal.fragment", "Shaders\\gerstner_normal.geometry"});
	shader_pool.insert(std::pair<std::string, std::unique_ptr<Shader>>(temp->renderer_type, std::unique_ptr<Shader>(temp)));


}