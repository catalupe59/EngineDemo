#pragma once

#include <ctime>
class Time {
private:
	static double start_time;
	static double elapsed_time;
	static double global_elapsed_time;
public:
	static void register_end_update();
	static void register_start_update();
	static double delta_time();
	static double global_time();
};

