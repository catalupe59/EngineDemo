#include "stdafx.h"
#include "Time.h"

double Time::start_time = 0.0;
double Time::elapsed_time = 0.0;
double Time::global_elapsed_time = 0.0;

void Time::register_start_update() {
	start_time = glfwGetTime();
}

void Time::register_end_update() {
	elapsed_time = glfwGetTime() - start_time;
	global_elapsed_time += elapsed_time;
}

double Time::delta_time() {
	return elapsed_time;
}

double Time::global_time() {
	return global_elapsed_time;
}