#pragma once
class Mesh;
class Single_Mesh;
class Material;
class Shader;
class Resource {
private:
	static std::map<std::string, std::unique_ptr<Mesh>> mesh_pool;
	static std::map<std::string, std::unique_ptr<Material>> material_pool;
	static std::map<std::string, std::unique_ptr<Shader>> shader_pool;

public:
	static void init();

	static Shader* get_shader(const std::string& name);
	static Mesh* get_mesh(const std::string& name);
	static Material* get_material(const std::string& name);

	static std::map<std::string, std::unique_ptr<Mesh>>& get_mesh_pool();

	static void insert_material(const std::string name, Material* material);
	static void reload_shader(const std::string & name);

private:
	static void init_shaders();
	static void init_meshes();
	static void init_materials();
};

