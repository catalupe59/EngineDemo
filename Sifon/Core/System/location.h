#pragma once
class Location {
public:
	static std::map<std::string,std::map<std::string,GLuint>> uniform_location;
	static std::map<std::string,std::map<std::string,GLuint>> attribute_location;

	static GLuint get_uniform_location(std::string& shader, std::string& name);
	static GLuint get_uniform_location(std::string& shader, const char name[]);
};

