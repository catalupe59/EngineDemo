#pragma once
class Input_Manager {
public:

private:
	static std::vector<bool> key;
	static glm::vec3 mouse_pos;
private:
	static glm::vec3 delta;
	static glm::vec3 interm_delta;
public:
	static bool tapped(int button);
	static bool pressed(int button);
	static glm::vec3& delta_mouse();

	static void key_callback(GLFWwindow* window, int key, int scancode, int action, int mods);
	static void mouse_button_callback(GLFWwindow* window, int button, int action, int mods);
	static void mouse_pos_callback(GLFWwindow* window, double xpos, double ypos);
};