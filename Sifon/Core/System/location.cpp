#include "stdafx.h"
#include "location.h"
#include "Core\Graphics\shaders.h"
#include "Core\System\resource.h"

std::map<std::string, std::map<std::string, GLuint>> Location::uniform_location;
std::map<std::string, std::map<std::string, GLuint>> Location::attribute_location;

GLuint Location::get_uniform_location(std::string& shader, std::string& name) {
	auto it = uniform_location.find(shader);
	if (it != uniform_location.end()) {
		auto jt = it->second.find(name);
		if (jt != it->second.end()) {
			return jt->second;
		} else {
			GLuint loc = glGetUniformLocation(Resource::get_shader(shader)->getID(),name.c_str());
			it->second.insert(std::pair<std::string, GLuint>(name, loc));
			return loc;
		}
	} else {
		uniform_location.insert(std::pair < std::string, std::map<std::string, GLuint>>(shader, std::map<std::string, GLuint>()));
		return get_uniform_location(shader, name);
	}
}

GLuint Location::get_uniform_location(std::string& shader, const char name[]) {
	auto it = uniform_location.find(shader);
	if (it != uniform_location.end()) {
		auto jt = it->second.find(name);
		if (jt != it->second.end()) {
			return jt->second;
		}
		else {
			GLuint loc = glGetUniformLocation(Resource::get_shader(shader)->getID(), name);
			it->second.insert(std::pair<std::string, GLuint>(name, loc));
			return loc;
		}
	}
	else {
		uniform_location.insert(std::pair < std::string, std::map<std::string, GLuint>>(shader, std::map<std::string, GLuint>()));
		return get_uniform_location(shader, name);
	}
}