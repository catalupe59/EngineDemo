#pragma once
class Screen {
private:
	static glm::ivec2 screen_size;
public:
	static void set_screen(int width, int height);
	static glm::ivec2 get_screen_size();
};

