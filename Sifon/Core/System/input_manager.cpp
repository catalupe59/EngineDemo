#include "stdafx.h"
#include "input_manager.h"

std::vector<bool> Input_Manager::key = std::vector<bool>(500);
glm::vec3 Input_Manager::delta;
glm::vec3 Input_Manager::mouse_pos;
glm::vec3 Input_Manager::interm_delta;


bool Input_Manager::tapped(int button) {
	if (key[button]) {
		key[button] = false;
		return true;
	}
	return false;
}

bool Input_Manager::pressed(int button) {
	return key[button];
}

void Input_Manager::key_callback(GLFWwindow * window, int key, int scancode, int action, int mods) {
	if (action == GLFW_PRESS) {
		Input_Manager::key[key] = true;
	}
	else if (action == GLFW_RELEASE) {
		Input_Manager::key[key] = false;
	}
}

void Input_Manager::mouse_button_callback(GLFWwindow * window, int button, int action, int mods) {
	if (action == GLFW_PRESS) {
		key[button] = true;
	}
	else if (action == GLFW_RELEASE) {
		key[button] = false;
	}
	
}

void Input_Manager::mouse_pos_callback(GLFWwindow * window, double xpos, double ypos) {
	delta.x = mouse_pos.x - xpos;
	delta.y = mouse_pos.y - ypos;

	mouse_pos.x = xpos;
	mouse_pos.y = ypos;
}


glm::vec3 & Input_Manager::delta_mouse() {
	interm_delta = delta;
	delta.x = 0;
	delta.y = 0;
	return interm_delta;
}
