#include "stdafx.h"
#include "game_object.h"

#include "Extents\info.h"

#include "Core\Components\Shared_Components\material.h"
#include "Core\Graphics\shaders.h"
#include "Core\Graphics\frame_buffer.h"
#include "Core\Components\Unique_Components\transform.h"
#include "Core\Components\Unique_Components\camera.h"
#include "Core\Components\Unique_Components\light.h"

int GameObject::global_id = 0;

GameObject::GameObject() : 
	ID(global_id++), is_static(true), transform(nullptr),light(nullptr),camera(nullptr), mesh(nullptr),enabled(true){
}

GameObject::~GameObject() {}

void GameObject::set(bool enabled) {
	this->enabled = enabled;
}


int GameObject::get_ID() {
	return ID;
}

void GameObject::serialize(std::string file) {
	std::ofstream ofs(file, std::ofstream::out | std::ofstream::binary);
	ofs.write(version_id,sizeof(version_id));
	ofs.write((const char*)iteration, sizeof(iteration));
	ofs.close();
}

void GameObject::deserialize(std::string file) {
	std::ifstream ifs(file, std::ifstream::in | std::ifstream::binary);
	ifs.close();
}