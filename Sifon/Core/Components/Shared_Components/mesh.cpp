#include "stdafx.h"
#include "mesh.h"


const std::vector<glm::vec3> Mesh::DEFAULT_VERTICES = {
	glm::vec3(-0.5f, -0.5f, 0.0f),
	glm::vec3(0.5f, -0.5f, 0.0f),
	glm::vec3(0.5f, 0.5f, 0.0f),
	glm::vec3(-0.5f, 0.5f, 0.0f)
};

const std::vector<glm::vec3> Mesh::DEFAUL_TEXTURE_COORDINATES = {
	glm::vec3(0, 0, 0),
	glm::vec3(1, 0, 0),
	glm::vec3(1, 1, 0),
	glm::vec3(0, 1, 0),
};

const std::vector<glm::vec3> Mesh::DEFAULT_NORMALS = {
	glm::vec3(0,0,1),
	glm::vec3(0,0,1),
	glm::vec3(0,0,1),
	glm::vec3(0,0,1)
};

const std::vector<GLshort> Mesh::DEFAULT_INDICES = {
	0,1,2,3
};

Mesh::~Mesh() {
}

void Mesh::serialize(std::string location) {
}

void Mesh::deserialize(std::string location) {
}
