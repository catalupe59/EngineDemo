#pragma once
#include "Extents\iserializable.h"

class Mesh : public ISerializable{
protected:
	static const std::vector<glm::vec3> DEFAULT_VERTICES;
	static const std::vector<glm::vec3> DEFAUL_TEXTURE_COORDINATES;
	static const std::vector<glm::vec3> DEFAULT_NORMALS;
	static const std::vector<GLshort> DEFAULT_INDICES;
protected:
	GLuint VAO;
	GLuint VBO;
	GLuint IBO;

	std::vector<glm::vec3> vertices;
	std::vector<glm::vec3> texture_coordinates;
	std::vector<glm::vec3> normals;
	std::vector<glm::vec3> tangents;
	std::vector<glm::vec3> bitangents;
	std::vector<GLshort> indices;

	std::vector<glm::vec3> accum_buffer;

public:
	inline GLuint getIBO() const {
		return IBO;
	};
	inline GLuint getVBO() const {
		return VBO;
	}
	inline GLuint getVAO() const {
		return VAO;
	}
	
	inline std::vector<glm::vec3>& get_accum_buffer() { return accum_buffer; }
	inline std::vector<GLshort>& get_indices() { return indices; }
	inline std::vector<glm::vec3>& get_vertices() { return vertices; }
	inline std::vector<glm::vec3>& get_texture_coords() { return texture_coordinates; }
	inline std::vector<glm::vec3>& get_tangets() { return tangents; }
	inline std::vector<glm::vec3>& get_bitangets() { return bitangents; }
	inline std::vector<glm::vec3>& get_normals() { return normals; }

	virtual ~Mesh() = 0;
	virtual void render(GLint draw_mode) = 0;

	void serialize(std::string location) override;
	void deserialize(std::string location) override;
};

