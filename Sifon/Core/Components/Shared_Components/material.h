#pragma once

#include "Extents\iserializable.h"

class Shader;
class Camera;
class FrameBuffer;
class Base_Texture;
class Bezier_Curve;

class Material : public ISerializable {
private:
	std::vector<std::shared_ptr<Base_Texture>> textures;
public:
	Material(const std::vector<std::string>& location);
	Material(const std::shared_ptr<Base_Texture> texture = nullptr);
	Material(std::unique_ptr<FrameBuffer>& framebuffers);
	
	void serialize(std::string location) override;
	void deserialize(std::string location) override;

	std::vector<std::shared_ptr<Base_Texture>>& get_textures();

	~Material();
};

