#pragma once
#include "d:\Repos\Sifon\Sifon\Sifon\Core\Components\Shared_Components\mesh.h"
class Single_Mesh;
class Instanced_Mesh :public Mesh {
private:
	int initial_offset;
	std::vector<glm::vec3> positions;
public:
	Instanced_Mesh(Single_Mesh* mesh, std::vector<glm::vec3>& positions);
	~Instanced_Mesh();

	void render(GLint draw_mode)override;
};

