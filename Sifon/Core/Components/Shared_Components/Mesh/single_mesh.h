#pragma once
#include "Core\Components\Shared_Components\mesh.h"
class Single_Mesh : public Mesh {
public:
	~Single_Mesh();

	Single_Mesh(const std::vector<glm::vec3>& vertices,
				const std::vector<GLshort>& indices,
				const std::vector<glm::vec3>& normals = {},
				const std::vector<glm::vec3>& texture_coordinates = {},
				const std::vector<glm::vec3>& tangents = {},
				const std::vector<glm::vec3>& bitangents = {});

	Single_Mesh(std::string mdl_location);
	Single_Mesh();

	void render(GLint draw_mode)override;
};

