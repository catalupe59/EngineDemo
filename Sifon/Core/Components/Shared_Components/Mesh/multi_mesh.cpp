#include "stdafx.h"
#include "multi_mesh.h"

#include "Core\Components\Shared_Components\Mesh\single_mesh.h"

#define BUFFER_OFFSET(offset) (static_cast<char*>(0) + (offset))

Multi_Mesh::~Multi_Mesh() {
}

std::vector<int>& Multi_Mesh::get_indices_count() {
	return indices_count;
}


std::vector<GLvoid*>& Multi_Mesh::get_indices_offsets() {
	return indices_offsets;
}

int Multi_Mesh::get_count() const {
	return count;
}

void Multi_Mesh::render(GLint draw_mode) {
	glMultiDrawElements(draw_mode, get_indices_count().data(), GL_UNSIGNED_SHORT, get_indices_offsets().data(), get_count());
}

Multi_Mesh::Multi_Mesh(const std::vector<Single_Mesh*>& unbatched) : count(unbatched.size()) {
	/*
	{
		int vertices_size = 0;
		int indices_size = 0;
		for (int i = 0; i < count; i++) {
			vertices_size += unbatched[i]->get_vertices_data().size();
			indices_size += unbatched[i]->get_indices().size();
		}

		points.reserve(vertices_size);
		indices.reserve(indices_size);
	}

	indices_offsets.push_back(BUFFER_OFFSET(0));
	for (int i = 0; i < count; i++) {
		points.insert(points.end(), unbatched[i]->get_vertices_data().begin(), unbatched[i]->get_vertices_data().end());
		const int offset = indices.size();
		for (int j = 0; j < unbatched[i]->get_indices().size(); j++) {
			indices.push_back(unbatched[i]->get_indices()[j] + offset);
		}
		//indices.insert(indices.end(), unbatched[i]->indices.begin(), unbatched[i]->indices.end());
		indices_count.push_back(unbatched[i]->get_indices().size());
		int ind_cnt = 0;
		for (int j = 0; j <= i; j++) {
			ind_cnt += indices_count[j];
		}
		indices_offsets.push_back(BUFFER_OFFSET(ind_cnt * sizeof(GLushort)));
	}
	indices_offsets.pop_back();

	glGenVertexArrays(1, &VAO);
	glBindVertexArray(VAO);

	glGenBuffers(1, &VBO);
	glBindBuffer(GL_ARRAY_BUFFER, VBO);
	glBufferData(GL_ARRAY_BUFFER, sizeof(points[0])*points.size(), &points[0], GL_STATIC_DRAW);

	glGenBuffers(1, &IBO);
	glBindBuffer(GL_ELEMENT_ARRAY_BUFFER, IBO);
	glBufferData(GL_ELEMENT_ARRAY_BUFFER, sizeof(indices[0]) * indices.size(), &indices[0], GL_STATIC_DRAW);

	glBindBuffer(GL_ELEMENT_ARRAY_BUFFER, 0);
	glBindBuffer(GL_ARRAY_BUFFER, 0);
	glBindVertexArray(0);
	*/
}