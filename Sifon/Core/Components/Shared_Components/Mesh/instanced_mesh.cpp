#include "stdafx.h"
#include "instanced_mesh.h"

#include "Core\Components\Shared_Components\Mesh\single_mesh.h"

Instanced_Mesh::Instanced_Mesh(Single_Mesh * mesh, std::vector<glm::vec3>& positions)  :  positions(positions){
	this->vertices = mesh->get_vertices();
	this->normals = mesh->get_normals();
	this->texture_coordinates = mesh->get_texture_coords();
	this->tangents = mesh->get_tangets();
	this->bitangents = mesh->get_bitangets();
	this->accum_buffer = mesh->get_accum_buffer();
	this->indices = mesh->get_indices();
	initial_offset = accum_buffer.size();

	glGenVertexArrays(1, &VAO);
	glBindVertexArray(VAO);

	glGenBuffers(1, &VBO);
	glBindBuffer(GL_ARRAY_BUFFER, VBO);
	glBufferData(GL_ARRAY_BUFFER, sizeof(accum_buffer[0])*accum_buffer.size(), &accum_buffer[0], GL_STATIC_DRAW);

	glGenBuffers(1, &IBO);
	glBindBuffer(GL_ELEMENT_ARRAY_BUFFER, IBO);
	glBufferData(GL_ELEMENT_ARRAY_BUFFER, sizeof(indices[0]) *indices.size(), &indices[0], GL_STATIC_DRAW);

	glBindBuffer(GL_ELEMENT_ARRAY_BUFFER, 0);
	glBindBuffer(GL_ARRAY_BUFFER, 0);
	glBindVertexArray(0);
}

Instanced_Mesh::~Instanced_Mesh() {
}

void Instanced_Mesh::render(GLint draw_mode) {
	glEnableVertexAttribArray(5);
	glVertexAttribPointer(5, 3, GL_FLOAT, GL_FALSE, 0, (void*)(initial_offset*sizeof(GL_FLOAT)));

	glDrawElementsInstanced(draw_mode, indices.size(), GL_UNSIGNED_SHORT,0,positions.size());
}
