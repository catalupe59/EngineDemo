#include "stdafx.h"
#include "single_mesh.h"

#include "Importer.hpp"
#include "scene.h"
#include "postprocess.h"

Single_Mesh::~Single_Mesh() {}

Single_Mesh::Single_Mesh(const std::vector<glm::vec3>& vertices, 
						 const std::vector<GLshort>& indices, 
						 const std::vector<glm::vec3>& normals, 
						 const std::vector<glm::vec3>& texture_coordinates, 
						 const std::vector<glm::vec3>& tangents, 
						 const std::vector<glm::vec3>& bitangents){
	this->vertices = vertices;
	this->indices = indices;
	this->normals = normals;
	this->texture_coordinates = texture_coordinates;
	this->tangents = tangents;
	this->bitangents = bitangents;

	accum_buffer.insert(accum_buffer.end(), vertices.begin(), vertices.end());
	accum_buffer.insert(accum_buffer.end(), normals.begin(), normals.end());
	accum_buffer.insert(accum_buffer.end(), texture_coordinates.begin(), texture_coordinates.end());
	accum_buffer.insert(accum_buffer.end(), tangents.begin(), tangents.end());
	accum_buffer.insert(accum_buffer.end(), bitangents.begin(), bitangents.end());

	glGenVertexArrays(1, &VAO);
	glBindVertexArray(VAO);

	glGenBuffers(1, &VBO);
	glBindBuffer(GL_ARRAY_BUFFER, VBO);
	glBufferData(GL_ARRAY_BUFFER, sizeof(glm::vec3)*accum_buffer.size(), &accum_buffer[0].x, GL_STATIC_DRAW);
	glGenBuffers(1, &IBO);
	glBindBuffer(GL_ELEMENT_ARRAY_BUFFER, IBO);
	glBufferData(GL_ELEMENT_ARRAY_BUFFER, sizeof(indices[0]) *indices.size(), &indices[0], GL_STATIC_DRAW);

	glBindBuffer(GL_ELEMENT_ARRAY_BUFFER, 0);
	glBindBuffer(GL_ARRAY_BUFFER, 0);
	glBindVertexArray(0);
}

Single_Mesh::Single_Mesh(std::string mdl_location) {
	Assimp::Importer importer;
	const aiScene* scene = importer.ReadFile(mdl_location,
											 aiProcess_CalcTangentSpace |
											 aiProcess_Triangulate |
											 aiProcess_GenSmoothNormals |
											 aiProcess_SortByPType
	);

	for (int meshes = 0; meshes < scene->mNumMeshes; meshes++) {
		aiMesh* mesh = scene->mMeshes[meshes];

		for (unsigned int i = 0; i < mesh->mNumFaces; i++) {
			const aiFace& face = mesh->mFaces[i];
			indices.push_back(face.mIndices[0]);
			indices.push_back(face.mIndices[1]);
			indices.push_back(face.mIndices[2]);
		}

		for (unsigned int i = 0; i < mesh->mNumVertices; i++) {
			vertices.push_back(glm::vec3(mesh->mVertices[i].x, mesh->mVertices[i].y, mesh->mVertices[i].z));
			texture_coordinates.push_back(glm::vec3(mesh->mTextureCoords[0][i].x, 1.0f - mesh->mTextureCoords[0][i].y,0));
			normals.push_back(glm::vec3(mesh->mNormals[i].x, mesh->mNormals[i].y, mesh->mNormals[i].z));
			tangents.push_back(glm::vec3(mesh->mTangents[i].x, mesh->mTangents[i].y, mesh->mTangents[i].z));
			bitangents.push_back(glm::vec3(mesh->mBitangents[i].x, mesh->mBitangents[i].y, mesh->mBitangents[i].z));
		}
	}


	accum_buffer.insert(accum_buffer.end(), vertices.begin(), vertices.end());
	accum_buffer.insert(accum_buffer.end(), normals.begin(), normals.end());
	accum_buffer.insert(accum_buffer.end(),texture_coordinates.begin(), texture_coordinates.end());
	accum_buffer.insert(accum_buffer.end(),tangents.begin(), tangents.end());
	accum_buffer.insert(accum_buffer.end(),bitangents.begin(), bitangents.end());

	glGenVertexArrays(1, &VAO);
	glBindVertexArray(VAO);

	glGenBuffers(1, &VBO);
	glBindBuffer(GL_ARRAY_BUFFER, VBO);
	glBufferData(GL_ARRAY_BUFFER, sizeof(accum_buffer[0])*accum_buffer.size(), &accum_buffer[0], GL_STATIC_DRAW);

	glGenBuffers(1, &IBO);
	glBindBuffer(GL_ELEMENT_ARRAY_BUFFER, IBO);
	glBufferData(GL_ELEMENT_ARRAY_BUFFER, sizeof(indices[0]) *indices.size(), &indices[0], GL_STATIC_DRAW);

	glBindBuffer(GL_ELEMENT_ARRAY_BUFFER, 0);
	glBindBuffer(GL_ARRAY_BUFFER, 0);
	glBindVertexArray(0);
}

void Single_Mesh::render(GLint draw_mode) {
	glDrawElements(draw_mode, indices.size(), GL_UNSIGNED_SHORT, 0);
}

Single_Mesh::Single_Mesh() : Single_Mesh(DEFAULT_VERTICES, DEFAULT_INDICES) {

}