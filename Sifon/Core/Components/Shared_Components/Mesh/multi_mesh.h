#pragma once
#include "d:\Repos\Sifon\Sifon\Sifon\Core\Components\Shared_Components\mesh.h"

class Single_Mesh;
class Multi_Mesh :public Mesh {
private:
	int count;
	std::vector<int> indices_count;
	std::vector<GLvoid*> indices_offsets;
public:
	Multi_Mesh(const std::vector<Single_Mesh*>& unbatched);
	~Multi_Mesh();

	std::vector<int>& get_indices_count();
	std::vector<GLvoid*>& get_indices_offsets();
	int get_count() const;

	void render(GLint draw_mode)override;

};

