#include "stdafx.h"
#include "material.h"

#include "Core\Graphics\Texture\base_texture.h"
#include "Core\Graphics\Texture\color_texture.h"
#include "Core\Graphics\frame_buffer.h"

Material::Material(const std::vector<std::string>& locations) {
	for (int i = 0; i < locations.size(); i++) {
		textures.push_back(std::shared_ptr<Base_Texture>(DBG_NEW Color_Texture(locations[i])));
	}
}

Material::Material(std::shared_ptr<Base_Texture> texture) {
	if (texture) {
		textures.push_back(texture);
	}
}

Material::Material(std::unique_ptr<FrameBuffer>& framebuffers) {
	textures.push_back(framebuffers->get_texture());
}

void Material::serialize(std::string location) {}

void Material::deserialize(std::string location) {}

std::vector<std::shared_ptr<Base_Texture>>& Material::get_textures() {
	return textures;
}

Material::~Material() {}
