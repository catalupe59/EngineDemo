#pragma once

#include "Extents\iserializable.h"

class Shader;
class Camera;
class FrameBuffer;
enum Render_Type;

class Transform : public ISerializable {
protected:
	const static std::unique_ptr<glm::vec3> forward;
	const static std::unique_ptr<glm::vec3> upward;
	const static std::unique_ptr<glm::vec3> rightward;
protected:
	std::unique_ptr<glm::vec3> position;
	std::unique_ptr<glm::vec3> size;
	std::unique_ptr<glm::quat> rotation;

	std::unique_ptr<glm::vec3> front;
	std::unique_ptr<glm::vec3> up;
	std::unique_ptr<glm::vec3> right;

	glm::mat4 mvp;
	
	std::unique_ptr<glm::mat4> position_matrix;
	std::unique_ptr<glm::mat4> size_matrix;
	std::unique_ptr<glm::mat4> rotation_matrix;
public:
	Transform(glm::vec3& position = glm::vec3(0),
			  glm::vec3& scale = glm::vec3(1), 
			  glm::vec3& rotation = glm::vec3(0));
	~Transform();

	glm::mat4& getMVP();
	inline glm::mat4 get_inverse_MVP() { return glm::inverse(getMVP()); }
	inline glm::vec3* get_position() { return position.get(); }
	inline glm::vec3* get_size() { return size.get(); }
	glm::vec3 get_euler_angles();

	virtual void translate(glm::vec3& translation);
	virtual void translate_forward(float x);
	virtual void translate_right(float x);
	virtual void translate_local_forward(float x);
	virtual void translate_local_right(float x);
	virtual void scale(glm::vec3& scale);
	virtual void rotate(glm::quat& rotation);

	virtual void set_rotation(glm::quat& rotation);
	inline void set_position(glm::vec3& position) { *this->position = position; update(); }
	inline void set_scale(glm::vec3& size) { *this->size = size; update(); }
	
	void update();

	void serialize(std::string location) override;
	void deserialize(std::string location) override;


};
