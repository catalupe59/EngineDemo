#pragma once

#include "Extents\iserializable.h"

class Camera : public ISerializable{
public:
private:
	bool orthographic;
	float aspect;
	float size;
	float near;
	float far;

	glm::mat4 projection_matrix;
	glm::mat4 inverse_projection_matrix;
	void calculate_projection_matrix();
public:
	Camera(float size, bool orthographic);
	~Camera();
	void set_orthographic();
	void set_perspective();
	bool is_perspective();
	bool is_orthographic();
	bool can_see(glm::vec3& position);

	void set_size(float size);

	glm::mat4 get_projection_matrix();
	glm::mat4 get_inverse_projection_matrix();

	void deserialize(std::string location) override;
	void serialize(std::string location) override;

};

