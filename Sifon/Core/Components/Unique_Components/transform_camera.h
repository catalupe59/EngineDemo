#pragma once

#include "Core\Components\Unique_Components\transform.h"

class Transform_Camera : public Transform{
public:
	Transform_Camera();
	~Transform_Camera();

	void translate(glm::vec3& translation) override;
	void scale(glm::vec3& scale) override;
	void rotate(glm::quat& rotation) override;
	void set_rotation(glm::quat & rotation) override;
};

