#include "stdafx.h"
#include "camera.h"

#include "gtc\matrix_transform.hpp"
#include "Core\System\screen.h"
#include "Core\Components\Unique_Components\transform.h"


Camera::Camera(float size, bool orthographic):
size(size), orthographic(orthographic) {
	calculate_projection_matrix();
}

Camera::~Camera() {
}

void Camera::set_orthographic() {
	orthographic = true;
}
void Camera::set_perspective() {
	orthographic = false;
}

bool Camera::is_perspective() {
	return !orthographic;
}
bool Camera::is_orthographic() {
	return orthographic;
}

void Camera::set_size(float size) {
	this->size = size;
	calculate_projection_matrix();
}
glm::mat4 Camera::get_projection_matrix() {
	return projection_matrix;
}
glm::mat4 Camera::get_inverse_projection_matrix() {
	return inverse_projection_matrix;
}
void Camera::deserialize(std::string location) {
}
void Camera::serialize(std::string location) {
}
void Camera::calculate_projection_matrix() {
	this->aspect = (float)Screen::get_screen_size().x/Screen::get_screen_size().y;
	near = 0.1f;
	far = 100.0f;
	if (orthographic) {
		float width = size * aspect;
		projection_matrix = glm::ortho(-width/2.0f,width/2.0f,-size/2.0f, size/2.0f,near, far);
		inverse_projection_matrix = glm::inverse(projection_matrix);
	} else {
		projection_matrix = glm::perspective(size,aspect, near, far);
		inverse_projection_matrix = glm::inverse(projection_matrix);
	}
}

bool Camera::can_see(glm::vec3& position) {

	return false;	
}