#pragma once
#include "Extents\iserializable.h"

class Light : public ISerializable {
private:
	glm::vec3 color;
	float power;
	float length;
public:
	Light();
	Light(glm::vec3& color, float power = 1.0f, float length = 1.0f);
	~Light();
	void serialize(std::string location)override;
	void deserialize(std::string location)override;
public:
	inline float get_power()const { return power; }
	inline float get_length()const { return length; }
	inline glm::vec3& get_color(){ return color; }
};

