#include "stdafx.h"
#include "transform_camera.h"
#include "gtx\quaternion.hpp"


Transform_Camera::Transform_Camera() {
}


Transform_Camera::~Transform_Camera() {
}

void Transform_Camera::translate(glm::vec3 & translation) {
	*position = *position - translation;
	update();

}

void Transform_Camera::scale(glm::vec3 & scale) {
	*size = *size - scale;
	update();
}

void Transform_Camera::rotate(glm::quat & rotate) {
	*rotation = glm::inverse(rotate) * *rotation;

	*front = *rotation * *forward;
	*up = *rotation * *upward;
	*right = *rotation * *rightward;

	update();
}


void Transform_Camera::set_rotation(glm::quat & rotation) {
	*this->rotation = rotation;
	//*this->rotation = glm::inverse(rotation);
	

	*front = *this->rotation * *forward;
	*up = *this->rotation * *upward;
	*right = *this->rotation * *rightward;

	update();
}
