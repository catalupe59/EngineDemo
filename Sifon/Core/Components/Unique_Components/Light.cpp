#include "stdafx.h"
#include "light.h"


Light::Light() : color(glm::vec3(1,1,1)), power(1.0f), length(1.0f) {}
Light::Light(glm::vec3 & color, float power, float length) :color(color) ,power(power), length(length) {}
Light::~Light() {}

void Light::serialize(std::string location) {}
void Light::deserialize(std::string location) {}
