#include "stdafx.h"
#include "transform.h"
#include "gtc\matrix_transform.hpp"
#include "gtx\quaternion.hpp"

const std::unique_ptr<glm::vec3> Transform::forward = std::make_unique<glm::vec3>(std::move(glm::vec3(0, 0, 1)));
const std::unique_ptr<glm::vec3> Transform::upward = std::make_unique<glm::vec3>(std::move(glm::vec3(0, 1, 0)));
const std::unique_ptr<glm::vec3> Transform::rightward = std::make_unique<glm::vec3>(std::move(glm::vec3(1, 0, 0 )));

Transform::Transform(glm::vec3& position,
					 glm::vec3& scale,
					 glm::vec3& rotation) :
	position(std::unique_ptr<glm::vec3>(DBG_NEW glm::vec3(position))),
	size(std::unique_ptr<glm::vec3>(DBG_NEW glm::vec3(scale))),
	rotation(std::unique_ptr<glm::quat>(DBG_NEW glm::quat(rotation))),
	front(std::unique_ptr<glm::vec3>(DBG_NEW glm::vec3(*forward))),
	up(std::unique_ptr<glm::vec3>(DBG_NEW glm::vec3(*upward))),
	right(std::unique_ptr<glm::vec3>(DBG_NEW glm::vec3(*rightward))){

	position_matrix = std::unique_ptr<glm::mat4>(DBG_NEW glm::mat4(1));
	rotation_matrix= std::unique_ptr<glm::mat4>(DBG_NEW glm::mat4(1));
	size_matrix = std::unique_ptr<glm::mat4>(DBG_NEW glm::mat4(1));
	update();
}

Transform::~Transform() {
}


void Transform::update() {
	*position_matrix = glm::translate(glm::mat4(1), *position);
	*size_matrix = glm::scale(glm::mat4(1), *size);
	*rotation_matrix = glm::toMat4(*rotation);

	mvp = glm::mat4(1);
	mvp *= *rotation_matrix;
	mvp *= *position_matrix;
	mvp *= *size_matrix;

}

void Transform::deserialize(std::string location) {
}


void Transform::serialize(std::string location) {
}

glm::mat4& Transform::getMVP() {
	return mvp;
}


glm::vec3 Transform::get_euler_angles() {
	return glm::eulerAngles(*rotation);
}

void Transform::translate(glm::vec3& translation) {
	*position = *position + translation;
	update();
}

void Transform::translate_forward(float x) {
	translate(*forward*x);
}

void Transform::translate_right(float x) {
	translate(*rightward*x);
}

void Transform::translate_local_forward(float x) {
	translate(*front*x);
}

void Transform::translate_local_right(float x) {
	translate(*right*x);
}

void Transform::scale(glm::vec3& scale) {
	*size = * size + scale;
	update();
}
void Transform::rotate(glm::quat& rotate) {
	*rotation = rotate * *rotation ;

	*front = glm::vec3(glm::toMat4(*rotation) * glm::vec4(*forward,1));
	*up= glm::vec3(glm::toMat4(*rotation) * glm::vec4(*upward, 1));
	*right = glm::vec3(glm::toMat4(*rotation) * glm::vec4(*rightward, 1));

	update();
}

void Transform::set_rotation(glm::quat & rotation) {
	*this->rotation = rotation;

	*front = *this->rotation * *forward;
	*up = *this->rotation * *upward;
	*right = *this->rotation * *rightward;

	update();
}
