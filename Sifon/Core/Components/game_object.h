#pragma once

#include "Extents\iserializable.h"

class Shader;
class Camera;
class FrameBuffer;
class Transform;
class Material;
class Mesh;
class Light;


class GameObject : public ISerializable {
private:
	static int global_id;
public:
	std::unique_ptr<Transform> transform;
	std::unique_ptr<Camera> camera;
	std::unique_ptr<Light> light;
	Material* material;
	Mesh* mesh;
	bool is_static;
private:
	int ID;
public:
	GameObject();
	~GameObject();

	void set(bool enabled);
	int get_ID();
	bool enabled;

	void serialize(std::string file) override;
	void deserialize(std::string file) override;
};
