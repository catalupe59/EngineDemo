#include "stdafx.h"
#include "frame_buffer.h"
#include "Core\System\screen.h"
#include "Core\Graphics\Texture\base_texture.h"
#include "Core\Graphics\Texture\double_color_depth_texture.h"

std::unique_ptr<FrameBuffer> FrameBuffer::SCREEN = std::unique_ptr<FrameBuffer>(DBG_NEW FrameBuffer(0));
std::vector<GLuint> FrameBuffer::frame_buffer_pool;

GLuint FrameBuffer::getBuffer() {
	return frame_buffer;
}


FrameBuffer::FrameBuffer() {
	glGenFramebuffers(1, &frame_buffer);
	frame_buffer_pool.push_back(frame_buffer);
	glBindFramebuffer(GL_FRAMEBUFFER, frame_buffer);

	texture = std::shared_ptr<Base_Texture>(DBG_NEW Double_Color_Depth_Texture());

	glFramebufferTexture2D(GL_FRAMEBUFFER, GL_COLOR_ATTACHMENT0, GL_TEXTURE_2D, (*texture)[0], 0);
	glFramebufferTexture2D(GL_FRAMEBUFFER, GL_DEPTH_ATTACHMENT, GL_TEXTURE_2D, (*texture)[1], 0);
	glFramebufferTexture2D(GL_FRAMEBUFFER, GL_COLOR_ATTACHMENT1, GL_TEXTURE_2D, (*texture)[2], 0);

	const GLenum buffers[] = { GL_COLOR_ATTACHMENT0, GL_COLOR_ATTACHMENT1};
	glDrawBuffers(2, buffers);
	

	const GLenum status = glCheckFramebufferStatus(GL_FRAMEBUFFER);
	assert(status == GL_FRAMEBUFFER_COMPLETE);
	glBindFramebuffer(GL_FRAMEBUFFER, 0);
}

FrameBuffer::~FrameBuffer() {
	glDeleteFramebuffers(1,&frame_buffer);
}

void FrameBuffer::clear_framebuffers() {
	for (int i = 0; i < frame_buffer_pool.size(); i++) {
		glBindFramebuffer(GL_FRAMEBUFFER,frame_buffer_pool[i]);
		glClearColor(0,0,0,0);
		glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT);
	}
	glBindFramebuffer(GL_FRAMEBUFFER, 0);
}

void FrameBuffer::bind() {
	glBindFramebuffer(GL_FRAMEBUFFER, frame_buffer);
}
void FrameBuffer::unbind() {
	glBindFramebuffer(GL_FRAMEBUFFER, 0);
}

std::shared_ptr<Base_Texture> FrameBuffer::get_texture(){
	return texture;
}

FrameBuffer::FrameBuffer(GLuint location) : frame_buffer(location) {}