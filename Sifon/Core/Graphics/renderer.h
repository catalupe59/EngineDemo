#pragma once

enum Renderer_Type;
class Shader;
class Light;
class GameObject;
class FrameBuffer;

class Renderer{

public:
	static void render(GameObject* gameobject, GLint draw_mode, Shader* shader, GameObject* camera, FrameBuffer* framebuffer, GameObject& global_light);
	static void render_points(GameObject* gameobject, GLint draw_mode, Shader* shader, GameObject* camera, FrameBuffer* framebuffer);

	Renderer();
	~Renderer();
};

