#pragma once

#include "Core\Graphics\character.h"

class Font {
public:
	static std::map<std::string, std::map<GLchar,Character>> font;
	static void load_font(std::string location);
};

