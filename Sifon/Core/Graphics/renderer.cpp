#include "stdafx.h"
#include "renderer.h"

//#include "Core\Graphics\texture.h"
#include "Core\Graphics\Texture\base_texture.h"
#include "Core\Components\Unique_Components\transform.h"
#include "Core\Components\Shared_Components\material.h"
#include "Core\Components\Shared_Components\mesh.h"
#include "Core\Components\game_object.h"
#include "Core\Graphics\shaders.h"
#include "Core\Components\Unique_Components\camera.h"
#include "Core\Graphics\frame_buffer.h"
#include "Core\System\Time.h"
#include "Core\System\location.h"
#include "Core\Components\Unique_Components\light.h"



void Renderer::render(GameObject* gameobject, GLint draw_mode, Shader* shader, GameObject* camera, FrameBuffer* framebuffer, GameObject& global_light) {
	assert(gameobject->transform);
	assert(gameobject->material);
	assert(gameobject->mesh);

	 Material* material = gameobject->material;
	 Mesh* mesh = gameobject->mesh;
	 Transform* transform = gameobject->transform.get();

	glUseProgram(shader->getID());
	framebuffer->bind();

	glBindVertexArray(mesh->getVAO());
	glBindBuffer(GL_ARRAY_BUFFER, mesh->getVBO());
	glEnableVertexAttribArray(0);
	glVertexAttribPointer(0, 3, GL_FLOAT, GL_FALSE, 0, 0);
	glEnableVertexAttribArray(1);
	glVertexAttribPointer(1, 3, GL_FLOAT, GL_FALSE, 0, (void*)(mesh->get_normals().size() * sizeof(GL_FLOAT)));
	glEnableVertexAttribArray(2);
	glVertexAttribPointer(2, 3, GL_FLOAT, GL_FALSE, 0, (void*)(mesh->get_texture_coords().size() * sizeof(GL_FLOAT)));
	glEnableVertexAttribArray(3);//Tangents
	glVertexAttribPointer(3, 3, GL_FLOAT, GL_FALSE, 0, (void*)(mesh->get_tangets().size() * sizeof(GL_FLOAT)));
	glEnableVertexAttribArray(4);//Bitangets
	glVertexAttribPointer(4, 3, GL_FLOAT, GL_FALSE, 0, (void*)(mesh->get_bitangets().size() * sizeof(GL_FLOAT)));


	glBindBuffer(GL_ELEMENT_ARRAY_BUFFER, mesh->getIBO());
	
	glUniform1f(Location::get_uniform_location(shader->renderer_type, "time"), Time::global_time());
	glUniformMatrix4fv(Location::get_uniform_location(shader->renderer_type, "model_matrix"), 1, GL_FALSE, &transform->getMVP()[0][0]);
	if (camera) glUniformMatrix4fv(Location::get_uniform_location(shader->renderer_type, "camera_matrix"), 1, GL_FALSE, &camera->transform->getMVP()[0][0]);
	if (camera) glUniformMatrix4fv(Location::get_uniform_location(shader->renderer_type, "projection_matrix"), 1, GL_FALSE, &camera->camera->get_projection_matrix()[0][0]);
	glUniform3f(Location::get_uniform_location(shader->renderer_type, "global_light"), global_light.transform->get_position()->x, global_light.transform->get_position()->y, global_light.transform->get_position()->z);

	int k = 0;
	for (int i = 0; i < material->get_textures().size(); i++) {
		for (int j = 0; j < material->get_textures()[i]->buffers.size(); j++) {
			glActiveTexture(GL_TEXTURE0 + k);
			material->get_textures()[i]->bind(j);
			glUniform1i(Location::get_uniform_location(shader->renderer_type,"txt"+std::to_string(k)),k);
			k++;
		}
	}
	mesh->render(draw_mode);

	glBindTexture(GL_TEXTURE_2D, 0);
	glBindBuffer(GL_ARRAY_BUFFER, 0);
	glBindBuffer(GL_ELEMENT_ARRAY_BUFFER, 0);
	glDisableVertexAttribArray(0);
	glDisableVertexAttribArray(1);
	glDisableVertexAttribArray(2);
	glDisableVertexAttribArray(3);
	glDisableVertexAttribArray(4);
	glBindVertexArray(0);
	framebuffer->unbind();
	glUseProgram(0);
}


Renderer::Renderer() {
}


Renderer::~Renderer() {
}

void Renderer::render_points(GameObject* gameobject, GLint draw_mode, Shader* shader, GameObject* camera, FrameBuffer* framebuffer) {
	glPointSize(7.0f);
	glLineWidth(1.0f);
	Mesh* mesh = gameobject->mesh;
	Transform* transform = gameobject->transform.get();

	glUseProgram(shader->getID());
	framebuffer->bind();

	glBindVertexArray(mesh->getVAO());
	glBindBuffer(GL_ARRAY_BUFFER, mesh->getVBO());
	glEnableVertexAttribArray(0);
	glVertexAttribPointer(0, 2, GL_FLOAT, GL_FALSE, 2 * sizeof(GL_FLOAT), 0);
	glBindBuffer(GL_ELEMENT_ARRAY_BUFFER, mesh->getIBO());
	mesh->render(draw_mode);
	glBindTexture(GL_TEXTURE_2D, 0);
	glBindBuffer(GL_ARRAY_BUFFER, 0);
	glBindBuffer(GL_ELEMENT_ARRAY_BUFFER, 0);
	glDisableVertexAttribArray(0);
	glBindVertexArray(0);
	framebuffer->unbind();
	glUseProgram(0);
}