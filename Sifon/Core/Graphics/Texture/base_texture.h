#pragma once

class Base_Texture {
public:
	std::vector<GLuint> buffers;
protected:
	Base_Texture(int textures);
	Base_Texture(std::string location);
public:
	void bind(int index);
	void unbind();
	virtual ~Base_Texture();
	GLuint operator[](int index);
};

