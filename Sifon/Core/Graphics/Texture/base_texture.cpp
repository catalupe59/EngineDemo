#include "stdafx.h"
#include "base_texture.h"
#include "Core\System\screen.h"
#include "SOIL.h"


void Base_Texture::bind(int index) {
	glBindTexture(GL_TEXTURE_2D, buffers[index % buffers.size()]);
}

void Base_Texture::unbind() {
	glBindTexture(GL_TEXTURE_2D, 0);
}

Base_Texture::Base_Texture(int textures) {
	buffers.resize(textures, 0);
	glGenTextures(textures, buffers.data());
	for (int i = 0; i < textures; i++) {
		glActiveTexture(GL_TEXTURE0 + i);
		glBindTexture(GL_TEXTURE_2D, buffers[i]);
		glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_WRAP_S, GL_REPEAT);
		glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_WRAP_T, GL_REPEAT);
		glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MIN_FILTER, GL_LINEAR);
		glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MAG_FILTER, GL_LINEAR);
	}
	glBindTexture(GL_TEXTURE_2D, 0);

}

Base_Texture::Base_Texture(std::string location) {
	buffers.resize(1, 0);
	int width, height;
	unsigned char* image = SOIL_load_image(location.c_str(), &width, &height, 0, SOIL_LOAD_RGBA);
	assert(image != NULL);

	glGenTextures(1, &buffers[0]);
	glActiveTexture(GL_TEXTURE0);
	glBindTexture(GL_TEXTURE_2D, buffers[0]);

	glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_WRAP_S, GL_REPEAT);
	glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_WRAP_T, GL_REPEAT);
	glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MIN_FILTER, GL_LINEAR);
	glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MAG_FILTER, GL_LINEAR);
	glGenerateMipmap(GL_TEXTURE_2D);

	glTexImage2D(GL_TEXTURE_2D, 0, GL_RGBA, width, height, 0, GL_RGBA, GL_UNSIGNED_BYTE, image);
	glBindTexture(GL_TEXTURE_2D, 0);

	SOIL_free_image_data(image);
}
Base_Texture::~Base_Texture() {

}
GLuint Base_Texture::operator[](int index) {
	return buffers[index];
}