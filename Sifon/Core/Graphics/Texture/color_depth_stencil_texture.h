#pragma once
#include "base_texture.h"

class Color_Depth_Stencil_Texture : public Base_Texture {
public:
	Color_Depth_Stencil_Texture(int width, int height);
	Color_Depth_Stencil_Texture();
	~Color_Depth_Stencil_Texture();
};
