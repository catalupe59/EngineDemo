#include "stdafx.h"
#include "color_depth_texture.h"
#include "Core\System\screen.h"

Color_Depth_Texture::Color_Depth_Texture(int width, int height) : Base_Texture(2) {
	glBindTexture(GL_TEXTURE_2D, Base_Texture::buffers[0]);
	glTexImage2D(GL_TEXTURE_2D, 0, GL_RGBA, width, height, 0, GL_RGBA, GL_UNSIGNED_BYTE, nullptr);
	glBindTexture(GL_TEXTURE_2D, Base_Texture::buffers[1]);
	glTexImage2D(GL_TEXTURE_2D, 0, GL_DEPTH_COMPONENT, width, height, 0, GL_DEPTH_COMPONENT, GL_UNSIGNED_BYTE, nullptr);
	glBindTexture(GL_TEXTURE_2D, 0);
}

Color_Depth_Texture::Color_Depth_Texture() : Base_Texture(2) {
	glBindTexture(GL_TEXTURE_2D, Base_Texture::buffers[0]);
	glTexImage2D(GL_TEXTURE_2D, 0, GL_RGBA, Screen::get_screen_size().x, Screen::get_screen_size().y, 0, GL_RGBA, GL_UNSIGNED_BYTE, nullptr);
	glBindTexture(GL_TEXTURE_2D, Base_Texture::buffers[1]);
	glTexImage2D(GL_TEXTURE_2D, 0, GL_DEPTH_COMPONENT, Screen::get_screen_size().x, Screen::get_screen_size().y, 0, GL_DEPTH_COMPONENT, GL_UNSIGNED_BYTE, nullptr);
	glBindTexture(GL_TEXTURE_2D, 0);
}


Color_Depth_Texture::~Color_Depth_Texture() {
}
