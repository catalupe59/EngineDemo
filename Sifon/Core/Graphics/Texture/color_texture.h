#pragma once
#include "Core\Graphics\Texture\base_texture.h"
class Color_Texture : public Base_Texture {
public:
	Color_Texture(int width, int height);
	Color_Texture();
	Color_Texture(std::string location);
	~Color_Texture();
};

