#include "stdafx.h"
#include "depth_texture.h"
#include "Core\System\screen.h"

Depth_Texture::Depth_Texture(int width, int height) : Base_Texture(1) {
	glBindTexture(GL_TEXTURE_2D, Base_Texture::buffers[0]);
	glTexImage2D(GL_TEXTURE_2D, 0, GL_DEPTH_COMPONENT, width, height, 0, GL_DEPTH_COMPONENT, GL_UNSIGNED_BYTE, nullptr);
	glBindTexture(GL_TEXTURE_2D, 0);
}


Depth_Texture::Depth_Texture() : Base_Texture(1) {
	glBindTexture(GL_TEXTURE_2D, Base_Texture::buffers[0]);
	glTexImage2D(GL_TEXTURE_2D, 0, GL_DEPTH_COMPONENT, Screen::get_screen_size().x, Screen::get_screen_size().y, 0, GL_DEPTH_COMPONENT, GL_UNSIGNED_BYTE, nullptr);
	glBindTexture(GL_TEXTURE_2D, 0);
}

Depth_Texture::~Depth_Texture() {
}
