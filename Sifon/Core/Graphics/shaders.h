#pragma once
enum Renderer_Type;
class Shader {
public:
	std::vector<std::string> locations;
	std::string renderer_type;
private:
	GLuint shaderID;
private:
	Shader();
	static GLuint compile_vertex(std::string location);
	static GLuint compile_fragment(std::string loaction);
	static GLuint compile_geometry(std::string location);
public:
	~Shader();
	static Shader* create_shader(std::vector<std::string>& locations);
	GLuint getID();
};

