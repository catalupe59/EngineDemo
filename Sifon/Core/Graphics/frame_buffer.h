#pragma once

class Base_Texture;
class FrameBuffer {
public:
	static std::unique_ptr<FrameBuffer> SCREEN;
private:
	static std::vector<GLuint> frame_buffer_pool;
private:
	GLuint frame_buffer;
	std::shared_ptr<Base_Texture> texture;

private:
	FrameBuffer(GLuint location);
	
public:
	GLuint getBuffer();
	void bind();
	void unbind();
	std::shared_ptr<Base_Texture> get_texture();
	FrameBuffer();
	~FrameBuffer();
	
	/*
		CLEARS ALL FRAME BUFFERS AFTER RENDERING
	*/
	static void clear_framebuffers();
};

