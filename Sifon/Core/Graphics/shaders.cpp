#include "stdafx.h"
#include "shaders.h"
#include "..\Debug\debug.h"
#include "..\utils.h"

Shader* Shader::create_shader(std::vector<std::string>& locations) {
	Shader* shader = DBG_NEW Shader();
	shader->locations = locations;
	shader->shaderID = glCreateProgram();

	for (int i = 0; i < locations.size(); i++) {
		std::string name;
		std::stringstream extension(locations[i]);
		std::getline(extension,name,'\\');
		std::getline(extension, name, '.');
		shader->renderer_type = name;
		extension >> name;
		if (name == "vertex"){
			GLuint vertex_id = compile_vertex(locations[i]);
			glAttachShader(shader->shaderID, vertex_id);
		}else if (name == "fragment") {
			GLuint fragment_id = compile_fragment(locations[i]);
			glAttachShader(shader->shaderID, fragment_id);
		}else if (name == "geometry") {
			GLuint geometry_id = compile_geometry(locations[i]);
			glAttachShader(shader->shaderID, geometry_id);
		}
	}
	glLinkProgram(shader->shaderID);
	GLint isLinked = 0;
	glGetProgramiv(shader->shaderID, GL_LINK_STATUS, &isLinked);
	if (isLinked == GL_FALSE) {
		GLint maxLength = 0;
		glGetProgramiv(shader->shaderID, GL_INFO_LOG_LENGTH, &maxLength);

		//The maxLength includes the NULL character
		std::vector<GLchar> infoLog(maxLength);
		glGetProgramInfoLog(shader->shaderID, maxLength, &maxLength, &infoLog[0]);
		for (int i = 0; i < maxLength; i++) {
			std::cerr << infoLog[i];
		}
		std::cerr << "\n";
		glDeleteProgram(shader->shaderID);
	}
	assert(isLinked != GL_FALSE);

	return shader;
}

Shader::Shader() {

}


Shader::~Shader() {
  glDeleteProgram(shaderID);
}


GLuint Shader::getID() {
  return shaderID;
}


GLuint Shader::compile_vertex(std::string location) {
	GLuint vertexID = glCreateShader(GL_VERTEX_SHADER);
	const std::string code = Utils::load_text(location);

	const GLchar* source = (const GLchar*)code.c_str();
	glShaderSource(vertexID, 1, &source, 0);
	glCompileShader(vertexID);
	GLint isCompiled = 0;
	glGetShaderiv(vertexID, GL_COMPILE_STATUS, &isCompiled);
	if (isCompiled == GL_FALSE) {
		GLint maxLength = 0;
		glGetShaderiv(vertexID, GL_INFO_LOG_LENGTH, &maxLength);
		std::vector<GLchar> infoLog(maxLength);
		glGetShaderInfoLog(vertexID, maxLength, &maxLength, &infoLog[0]);
		for (int i = 0; i < maxLength; i++) {
			std::cerr << infoLog[i];
		}
		std::cerr << "\n";
		glDeleteShader(vertexID);
	}
	assert(isCompiled != GL_FALSE);
	return vertexID;
}

GLuint Shader::compile_fragment(std::string location) {
	GLuint fragmentID = glCreateShader(GL_FRAGMENT_SHADER);
	const std::string code = Utils::load_text(location);
	const GLchar* source = (const GLchar*)code.c_str();
	glShaderSource(fragmentID, 1, &source, 0);
	glCompileShader(fragmentID);
	GLint isCompiled = 0;
	glGetShaderiv(fragmentID, GL_COMPILE_STATUS, &isCompiled);
	if (isCompiled == GL_FALSE) {
		GLint maxLength = 0;
		glGetShaderiv(fragmentID, GL_INFO_LOG_LENGTH, &maxLength);
		std::vector<GLchar> infoLog(maxLength);
		glGetShaderInfoLog(fragmentID, maxLength, &maxLength, &infoLog[0]);
		for (int i = 0; i < maxLength; i++) {
			std::cerr << infoLog[i];
		}
		std::cerr << "\n";
		glDeleteShader(fragmentID);
	}
	assert(isCompiled != GL_FALSE);
	return fragmentID;
}

GLuint Shader::compile_geometry(std::string location) {
	GLuint geometryID = glCreateShader(GL_GEOMETRY_SHADER);
	const std::string code = Utils::load_text(location);
	const GLchar* source = (const GLchar*)code.c_str();
	glShaderSource(geometryID, 1, &source, 0);
	glCompileShader(geometryID);
	GLint isCompiled = 0;
	glGetShaderiv(geometryID, GL_COMPILE_STATUS, &isCompiled);
	if (isCompiled == GL_FALSE) {
		GLint maxLength = 0;
		glGetShaderiv(geometryID, GL_INFO_LOG_LENGTH, &maxLength);
		std::vector<GLchar> infoLog(maxLength);
		glGetShaderInfoLog(geometryID, maxLength, &maxLength, &infoLog[0]);
		for (int i = 0; i < maxLength; i++) {
			std::cerr << infoLog[i];
		}
		std::cerr << "\n";
		glDeleteShader(geometryID);
	}
	assert(isCompiled != GL_FALSE);
	return geometryID;
}