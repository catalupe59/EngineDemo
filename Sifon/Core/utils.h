#pragma once
class Utils {
public:
	static std::string load_text(std::string path);
	static glm::vec3 lerp_color(glm::vec3& color_a, glm::vec3& color_b, float t);
};

