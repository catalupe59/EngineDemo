#include "stdafx.h"
#include "utils.h"
#include "Debug\debug.h"

std::string Utils::load_text(std::string path) {
	std::ifstream fileStream;
	fileStream.open(path, std::ios::in);
	std::string buffer;
	std::string code;
	if (fileStream.is_open()) {
		while (std::getline(fileStream, buffer)) {
			code += buffer;
			code += "\n";
		}
	}
	else {
		Debug::log("File " + path + " not found");
	}
	return code;
}

glm::vec3 Utils::lerp_color(glm::vec3& color_a, glm::vec3& color_b, float t) {
	return glm::vec3(color_a.r + (color_b.r - color_a.r) * t,
					 color_a.g + (color_b.g - color_a.g) * t,
					 color_a.b + (color_b.b - color_a.b) * t);
}
