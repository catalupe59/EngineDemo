#include "stdafx.h"
#include "program.h"
#include "Debug\tool_tip.h"
#include "Core\Level\scene.h"
#include "Core\Level\demo.h"
#include "Core\Graphics\Font.h"
#include "Core\System\resource.h"
#include "Core\System\screen.h"


Program::Program() {
	Resource::init();
	active_level = std::unique_ptr<Scene>(DBG_NEW Demo());
#ifdef _DEBUG
	Font::load_font("Fonts/opensans.ttf");
	tool_tip = std::make_unique<Tool_Tip>(Tool_Tip());
#endif
	running = true;
}

Program::~Program() {
}

void Program::run() {
	Time::register_start_update();
	update();
	render();

	Time::register_end_update();
}

void Program::update() {
	GLenum error = glGetError();
	while (error != GL_NO_ERROR) {
		const unsigned char * errorstring = glewGetErrorString(error);
		std::cout << errorstring << error << "\n";
		error = glGetError();
	}
	active_level->update();
#ifdef _DEBUG
	tool_tip->update();
#endif
}

void Program::init() {
	active_level->init();
}

void Program::render() {
#ifdef _DEBUG
	auto scr_size = Screen::get_screen_size();
	//glViewport(scr_size.x / 8, scr_size.y / 8, scr_size.x / 8 * 7, scr_size.y / 8 * 7);
#endif
	active_level->render();
#ifdef _DEBUG
	//glViewport(0, 0, scr_size.x, scr_size.y);
	tool_tip->render();
#endif
}



