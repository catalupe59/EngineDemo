#pragma once

#include "scene.h"

class FrameBuffer;
class Demo : public Scene {
private:
	std::unique_ptr<FrameBuffer> game_frame;
public:
	Demo();
	~Demo();

	void init() override;
	void update() override;
	void render() override;
};