#pragma once

#include "Extents/hierarchy.h"

class GameObject;
class Scene {
 public:
  virtual ~Scene();
 protected:
  Scene();
  Hierarchy hierarchy;
 public:
  virtual void update() = 0;
  virtual void init();
  virtual void render() = 0;
};

