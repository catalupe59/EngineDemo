#include "stdafx.h"
#include "demo.h"
#include "Extents/hierarchy.h"
#include "Core\Graphics\shaders.h"
#include "Core\Graphics\frame_buffer.h"
#include "Core\Components\Unique_Components\camera.h"
#include "Core\Components\Unique_Components\light.h"
#include "Core\System\input_manager.h"
#include "Core\Components\game_object.h"
#include "Core\Components\Unique_Components\transform.h"
#include "Core\Components\Unique_Components\transform_camera.h"
#include "Core\Components\Shared_Components\material.h"
#include "Core\Components\Shared_Components\Mesh\multi_mesh.h"
#include "Core\Components\Shared_Components\Mesh\single_mesh.h"
#include "Core\Components\Shared_Components\Mesh\instanced_mesh.h"
#include "Core\System\Time.h"
#include "Core\Graphics\renderer.h"
#include "Core\utils.h"
#include "Core\Graphics\character.h"
#include "Core\Scripts\map_generator.h"
#include "gtc\matrix_transform.hpp"
#include "Core\System\resource.h"
#include "gtx\quaternion.hpp"
#include <cmath>


Demo::Demo() : Scene() {
}


Demo::~Demo() {
	
}

void Demo::init(){

	game_frame = std::unique_ptr<FrameBuffer>(DBG_NEW FrameBuffer());
	Resource::insert_material("Frame_Buffer", DBG_NEW Material(game_frame));

	hierarchy.insert("Global_Light", DBG_NEW GameObject());
	hierarchy["Global_Light"].transform = std::unique_ptr<Transform> (DBG_NEW Transform());
	hierarchy["Global_Light"].light = std::unique_ptr<Light>(DBG_NEW Light());

	hierarchy.insert("Waves", DBG_NEW GameObject());
	hierarchy["Waves"].material = Resource::get_material("Floor");
	hierarchy["Waves"].mesh = Resource::get_mesh("Models/wave_quad.obj");
	hierarchy["Waves"].transform = std::unique_ptr<Transform>(DBG_NEW Transform());
	hierarchy["Waves"].transform->set_position(glm::vec3(0, -0.2, -1));


	hierarchy.insert("Camera", DBG_NEW GameObject());
	hierarchy["Camera"].camera = std::unique_ptr<Camera>(DBG_NEW Camera(1.04f, false));
	hierarchy["Camera"].transform = std::unique_ptr<Transform>(DBG_NEW Transform());

}

void Demo::update(){
	
	if (Input_Manager::pressed(GLFW_KEY_W)) {
		//hierarchy["Camera"].transform->translate_local_forward(0.01);
		hierarchy["Waves"].transform->translate_local_forward(0.01);
	}else if (Input_Manager::pressed(GLFW_KEY_S)) {
		//hierarchy["Camera"].transform->translate_local_forward(-0.01);
		hierarchy["Waves"].transform->translate_local_forward(-0.01);
	}

	if (Input_Manager::pressed(GLFW_KEY_D)) {
		//hierarchy["Camera"].transform->translate_local_right(-0.01);
		hierarchy["Waves"].transform->translate_local_right(-0.01);
	}
	else if (Input_Manager::pressed(GLFW_KEY_A)) {
		//hierarchy["Camera"].transform->translate_local_right(0.01);
		hierarchy["Waves"].transform->translate_local_right(0.01);
	}

	if (Input_Manager::pressed(GLFW_KEY_E)) {
		//hierarchy["Camera"].transform->rotate(glm::quat(glm::vec3(0.005, 0, 0)));
		hierarchy["Waves"].transform->rotate(glm::quat(glm::vec3(0.005, 0, 0)));
	}
}

void Demo::render() {
	//glCullFace(GL_BACK);
	glPointSize(2.0f);
	Renderer::render(&hierarchy["Waves"], GL_POINTS, Resource::get_shader("Gerstner"), &hierarchy["Camera"], FrameBuffer::SCREEN.get(), hierarchy["Global_Light"]);
	//Renderer::render(&hierarchy["Weapon"], GL_TRIANGLES, Resource::get_shader("PBR_instanced"), &hierarchy["Game_Camera"], game_frame.get(), hierarchy["Global_Light"]);
	//Renderer::render(&hierarchy["Floor"], GL_TRIANGLES, Resource::get_shader("bump_reflectivity"), &hierarchy["Game_Camera"], game_frame.get(), hierarchy["Global_Light"]);
	//Renderer::render(&hierarchy["Game_Quad"], GL_TRIANGLES, Resource::get_shader("Deffered_PBR"),nullptr, FrameBuffer::SCREEN.get(), hierarchy["Global_Light"]);
}