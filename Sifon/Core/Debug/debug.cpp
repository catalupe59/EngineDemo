#include "stdafx.h"
#include "debug.h"

void Debug::log(const std::string& string) {
	_log_file_(string);
	_log_console_(string);
}

void Debug::log(const char* string) {
	_log_file_(string);
	_log_console_(string);
}

void Debug::_log_console_(const std::string& string) {
	std::cout << string << "\n";
}

void Debug::_log_file_(const std::string& string) {
	//TODO
}

void Debug::_log_console_(const char* string) {
	std::cout << string << "\n";
}

void Debug::_log_file_(const char* string) {
	//TODO
}