#include "stdafx.h"
#include "tool_tip.h"

#include "Core\Components\Unique_Components\transform.h"
#include "Core\Components\Shared_Components\material.h"
#include "Core\Graphics\shaders.h"
#include "Core\Graphics\frame_buffer.h"
#include "Core\System\input_manager.h"
#include "Core\Components\game_object.h"
#include "Core\Graphics\renderer.h"

Tool_Tip::Tool_Tip(Tool_Tip& other) {
	screen = std::move(other.screen);
	items.resize(other.items.size());
	for (int i = 0; i < other.items.size(); i++) {
		items[i] = std::move(other.items[i]);
	}
	active = items.begin();
}

Tool_Tip::Tool_Tip() {
}

Tool_Tip::~Tool_Tip() {}

void Tool_Tip::render() {
	if (isVisible) {
	}
}
void Tool_Tip::update() {
	if (Input_Manager::tapped(GLFW_KEY_SPACE)) {
		if (isVisible) {
			isVisible = false;
		}
		else {
			isVisible = true;
		}
	}

	if (isVisible) {
		if (Input_Manager::tapped(GLFW_KEY_UP)) {
			active++;
			if (active == items.end()) {
				active = items.begin();
			}
		}
	}

}
