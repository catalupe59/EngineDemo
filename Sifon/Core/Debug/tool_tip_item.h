#pragma once
class Tool_Tip_Item {
private:
	std::string text;
	GLuint VAO, VBO;
public:
	void render(GLfloat x, GLfloat y, GLfloat scale, glm::vec3 color);
	Tool_Tip_Item(std::string& text);
	Tool_Tip_Item(const std::string text);
	~Tool_Tip_Item();
};

