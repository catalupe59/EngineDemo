#pragma once
class Debug {
private:
	static void _log_file_(const std::string& string);
	static void _log_console_(const std::string& string);
	static void _log_file_(const char* string);
	static void _log_console_(const char* string);
public:
	static void log(const std::string& string);
	static void log(const char* string);
};

