#pragma once

#include "Core\Debug\tool_tip_item.h"
class FrameBuffer;
class GameObject;

class Tool_Tip {
private:
	bool isVisible;
	std::unique_ptr<GameObject> screen;

	std::vector<std::unique_ptr<Tool_Tip_Item>> items;
	std::vector<std::unique_ptr<Tool_Tip_Item>>::iterator active;
public:
	void update();
	void render();
	Tool_Tip();
	Tool_Tip(Tool_Tip& other);
	~Tool_Tip();
};

//std::map<std::string, std::shared_ptr<Tool_Tip_Item>> variable_pool;
