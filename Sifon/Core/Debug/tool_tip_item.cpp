#include "stdafx.h"
#include "tool_tip_item.h"
#include "Core\Graphics\shaders.h"
#include "Core\Graphics\character.h"
#include "Core\Graphics\font.h"
#include "Core\System\Time.h"
#include "gtc\matrix_transform.hpp"
#include "Core\System\screen.h"

Tool_Tip_Item::Tool_Tip_Item(std::string& text) : text(text) {
	glGenVertexArrays(1, &VAO);
	glGenBuffers(1, &VBO);
	glBindVertexArray(VAO);
	glBindBuffer(GL_ARRAY_BUFFER, VBO);
	glBufferData(GL_ARRAY_BUFFER, sizeof(GLfloat) * 6 * 4, NULL, GL_DYNAMIC_DRAW);
	glEnableVertexAttribArray(0);
	glVertexAttribPointer(0, 4, GL_FLOAT, GL_FALSE, 4 * sizeof(GLfloat), 0);
	glBindBuffer(GL_ARRAY_BUFFER, 0);
	glBindVertexArray(0);
}

Tool_Tip_Item::Tool_Tip_Item(const std::string text) : text(text){
	glGenVertexArrays(1, &VAO);
	glGenBuffers(1, &VBO);
	glBindVertexArray(VAO);
	glBindBuffer(GL_ARRAY_BUFFER, VBO);
	glBufferData(GL_ARRAY_BUFFER, sizeof(GLfloat) * 6 * 4, NULL, GL_DYNAMIC_DRAW);
	glEnableVertexAttribArray(0);
	glVertexAttribPointer(0, 4, GL_FLOAT, GL_FALSE, 4 * sizeof(GLfloat), 0);
	glBindBuffer(GL_ARRAY_BUFFER, 0);
	glBindVertexArray(0);
}


Tool_Tip_Item::~Tool_Tip_Item() {}

void Tool_Tip_Item::render(GLfloat x, GLfloat y, GLfloat scale, glm::vec3 color) {
	// Activate corresponding render state
	Shader* shader = nullptr;//Resource::get_shader("Text");
	glUseProgram(shader->getID());
	glUniform3f(glGetUniformLocation(shader->getID(), "textColor"), color.x, color.y, color.z);
	glActiveTexture(GL_TEXTURE0);
	glBindVertexArray(VAO);

	//glm::mat4 projection_matrix = glm::ortho(-240.0f, 240.0f, -320.0f, 320.0f);
	glm::vec2 screen = Screen::get_screen_size();
	static const glm::mat4 projection_matrix = glm::ortho(-screen.x/2.0f, screen.x/2.0f, -screen.y/2.0f, screen.y/2.0f);

	glUniformMatrix4fv(glGetUniformLocation(shader->getID(), "projection_matrix"),1, GL_FALSE, &projection_matrix[0][0]);
	// Iterate through all characters
	std::string::const_iterator c;
	for (c = text.begin(); c != text.end(); c++) {
		Character ch = Font::font.at("Fonts/opensans.ttf").at(*c);

		GLfloat xpos = x + ch.Bearing.x * scale;
		GLfloat ypos = y - (ch.Size.y - ch.Bearing.y) * scale;

		GLfloat w = ch.Size.x * scale;
		GLfloat h = ch.Size.y * scale;
		// Update VBO for each character
		GLfloat vertices[6][4] = {
			{ xpos,     ypos + h,   0.0, 0.0 },
			{ xpos,     ypos,       0.0, 1.0 },
			{ xpos + w, ypos,       1.0, 1.0 },

			{ xpos,     ypos + h,   0.0, 0.0 },
			{ xpos + w, ypos,       1.0, 1.0 },
			{ xpos + w, ypos + h,   1.0, 0.0 }
		};
		// Render glyph texture over quad
		glBindTexture(GL_TEXTURE_2D, ch.TextureID);
		// Update content of VBO memory
		glBindBuffer(GL_ARRAY_BUFFER, VBO);
		glBufferSubData(GL_ARRAY_BUFFER, 0, sizeof(vertices), vertices);
		glBindBuffer(GL_ARRAY_BUFFER, 0);
		// Render quad
		glDrawArrays(GL_TRIANGLES, 0, 6);
		// Now advance cursors for next glyph (note that advance is number of 1/64 pixels)
		x += (ch.Advance >> 6) * scale; // Bitshift by 6 to get value in pixels (2^6 = 64)
	}
	glBindVertexArray(0);
	glBindTexture(GL_TEXTURE_2D, 0);
	glUseProgram(0);
}