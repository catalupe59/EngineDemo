#pragma once

#include "System\Time.h"
class Scene;
class Tool_Tip;
class Program {
public:
	std::unique_ptr<Scene> active_level;
	std::unique_ptr<Tool_Tip> tool_tip;
	bool running;
private:
	void update();
	void render();
public:
	Program();
	~Program();
	void init();
	void run();
};

