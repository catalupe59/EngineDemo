#pragma once
class Behaviour {
public:
	Behaviour();
	~Behaviour();

	virtual void init() = 0;
	virtual void update() = 0;
	virtual void render() = 0;
};

