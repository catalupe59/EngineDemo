#pragma once

class MapGenerator {
public:
	static std::vector<glm::vec2> generate_map(int, int, int seed);
private:
	static int neighbours_count(std::vector<std::vector<int>>&, int, int);
};
