#include "stdafx.h"
#include "map_generator.h"
#include <random>
#include <limits>

std::vector<glm::vec2> MapGenerator::generate_map(int width, int height,int seed) {
	std::default_random_engine generator(seed);
	std::uniform_int_distribution<int> building_distribution(2, 3);
	std::uniform_int_distribution<int> x_distribution(0, width);
	std::uniform_int_distribution<int> y_distribution(0, height);

	//Generate map
	std::vector<std::vector<int>> map;
	map.resize(height);
	for (int y = 0; y < map.size(); y++) {
		map[y].resize(width);
		for (int x = 0; x < map[y].size(); x++) {
				map[y][x] = 0;
		}
	}

	/*	Place buildings
		Keep track of % done
	*/
	float target_percent = 0.6f;
	float current_percent = 0.0f;

	while (current_percent <= target_percent) {
		//Generate building
		//Find spot to place it
		int b_width = building_distribution(generator);
		int b_height = building_distribution(generator);
		int x = x_distribution(generator);
		int y = y_distribution(generator);
		bool ok = true;
		for (int it_y = y - 2; it_y < y - 1 + b_height; it_y++) {
			for (int it_x = x - 2; it_x < x - 1 + b_width; it_x++) {
				try {
					if (map.at(it_y).at(it_x) == 1) {
						ok = false;
					}
				}catch (std::exception&) { ok = false; }
			}
		}
		if (ok) {
			for (int it_y = y - 2; it_y < y - 1 + b_height; it_y++) {
				for (int it_x = x - 2; it_x < x - 1 + b_width; it_x++) {
					if (it_y == y - 2 || it_y == y - 2 + b_height || it_x == x - 2 || it_x == x - 2 + b_width) {
					}else {
						map[it_y][it_x] = 1;
					}
				}
			}
			current_percent += static_cast<float>(b_height*b_width) / (width*height);
		}
	}

	for (int y = 0; y < map.size(); y++) {
		map[y][0] = 1;
		map[y][width-1] = 1;
	}
	for (int x = 0; x < map[0].size(); x++) {
		map[0][x] = 1;
		map[height-1][x] = 1;
	}

	for (int y = 0; y < map.size(); y++) {
		for (int x = 0; x < map[y].size(); x++) {
			std::cout << map[y][x] << " ";
		}
		std::cout << "\n";
	}

	std::vector<glm::vec2> pts;
	for (int y = 0; y < map.size(); y++) {
		for (int x = 0; x < map[y].size(); x++) {
			if (map[y][x] == 1) {
				pts.push_back(glm::vec2(x-(width/2),y-(height/2)));
			}
		}
	}
	return pts;
}

int MapGenerator::neighbours_count(std::vector<std::vector<int>>& map, int y, int x) {
	int count = 0;
	
	try {if (map.at(y+1).at(x) == 1) {count++;}}catch (std::exception&) {}
	try {if (map.at(y-1).at(x) == 1) {count++;}}catch (std::exception&) {}
	try {if (map.at(y).at(x+1) == 1) {count++;}}catch (std::exception&) {}
	try {if (map.at(y).at(x-1) == 1) {count++;}}catch (std::exception&) {}
	
	return count;
}
