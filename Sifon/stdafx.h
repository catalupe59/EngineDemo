// stdafx.h : include file for standard system include files,
// or project specific include files that are used frequently, but
// are changed infrequently
//
#pragma once

#define _CLIENT_
//#define _SERVER_


#ifdef _DEBUG
#define DBG_NEW new( _NORMAL_BLOCK , __FILE__ , __LINE__ )
// Replace _NORMAL_BLOCK with _CLIENT_BLOCK if you want the
// allocations to be of _CLIENT_BLOCK type
#else
#define DBG_NEW new
#endif
#define GLM_ENABLE_EXPERIMENTAL

#include "targetver.h"
#ifdef _DEBUG
#define _CRTDBG_MAP_ALLOC 
#endif

#include <memory>
#include <vector>
#include <iostream>
#include <fstream>
#include <string>
#include <map>
#include <unordered_map>
#include <typeindex>
#include <string>
#include <sstream>
#include <tuple>
#include <any>
#include <set>
#include <functional>
#include <stack>
#include <queue>
#include <algorithm>

#include <ft2build.h>
#include FT_FREETYPE_H 

#include "glm.hpp"
extern "C" {
#include <stdlib.h>  
#include <crtdbg.h>  
#include "glew.h"
#include "glfw3.h"
#include <stdio.h>
#include <string.h>
#include <math.h>
}

# define PI     3.14159265358979323846  /* pi */



// TODO: reference additional headers your program requires here
