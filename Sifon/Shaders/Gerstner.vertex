#version 330 core
layout(location = 0) in vec3 v_position;
layout(location = 1) in vec3 v_normal;

const float pi = 3.14159;
uniform mat4 model_matrix;
uniform mat4 camera_matrix;
uniform mat4 projection_matrix;
uniform float time;

out vec3 out_normal;

void main(){
	float amplitude[5] = float[](0.05, 0.03, 0.02, 0.01, 0.01);
	float wavelength[5] = float[](0.5, 0.8, 0.9, 0.1, 0.2);
	float w[5] = float[](2*pi/wavelength[0], 2*pi/wavelength[1] ,2*pi/wavelength[2], 2*pi/wavelength[3], 2*pi/wavelength[4]);
	float steepness[5] = float[](0.2,0.2, 0.3, 0.1, 0.2);

	float speed = 15.0;

	vec3 pos =  v_position;
	vec2 direction[5] = vec2[](vec2(1,0), vec2(0.5, 0.5), vec2(-0.5, 0), vec2(0.5,0), vec2(0.2, 0.8));
	float dotD[5] = float[](dot(pos.xz, direction[0]), dot(pos.xz, direction[1]) , dot(pos.xz, direction[2]), dot(pos.xz, direction[3]) , dot(pos.xz, direction[4]));
	float C[5] = float[](
						cos(w[0] * dotD[0] + time * speed),
						cos(w[1] * dotD[1] + time * speed),
						cos(w[2] * dotD[2] + time * speed),
						cos(w[3] * dotD[3] + time * speed),
						cos(w[4] * dotD[4] + time * speed)
						);
	float S[5] = float[](sin (w[0] * dotD[0] + time * speed), 
						sin (w[1] * dotD[1] + time * speed), 
						sin (w[2] * dotD[2] + time * speed),
						sin (w[3] * dotD[3] + time * speed),
						sin (w[4] * dotD[4] + time * speed));

	vec3 P = vec3(pos.x, 0 , pos.z);

	for (int i = 0; i < 5; i ++){
		P += vec3(steepness[i] * amplitude[i] * C[i] * direction[i].x,
					amplitude[i] * S[i], 
					steepness[i] * amplitude[i] * C[i] * direction[i].y);
	}
	
	out_normal = vec3(0,0,0);
	
	gl_Position =  projection_matrix * camera_matrix * model_matrix * vec4(P,1);
}

/*
void main(){
	out_normal = v_normal;
	float amplitude = 0.1;
	float wavelength = 0.4;
	float w = 2*pi/wavelength;
	float steepness = 0.5;

	float speed = 15.0;

	vec3 pos =  vec3(model_matrix * vec4(v_position,1));
	vec2 direction = vec2(1,0);
	float dotD = dot(pos.xz, direction);
	float C = cos(w * dotD + time * speed);
	float S = sin (w * dotD + time * speed);

	vec3 P = vec3(pos.x + steepness * amplitude * C * direction.x,
				amplitude * S, 
				pos.z + steepness * amplitude * C * direction.y);

	gl_Position =  projection_matrix * camera_matrix * vec4(P,1);
}*/